# [1.1.0](https://gitlab.souvap-univention.de/souvap/tooling/renovate-runner/compare/v1.0.0...v1.1.0) (2023-12-21)


### Bug Fixes

* **renovate:** Add file match pattern ([8369082](https://gitlab.souvap-univention.de/souvap/tooling/renovate-runner/commit/836908297269100238c2db2a9d491aa9e7967b80))


### Features

* **renovate:** Update dependency types ([73c371d](https://gitlab.souvap-univention.de/souvap/tooling/renovate-runner/commit/73c371dab93f11c1b3f72de00f920f7a63d4f7b6))

# 1.0.0 (2023-11-27)


### Bug Fixes

* Initial commit ([67ef7ee](https://gitlab.souvap-univention.de/souvap/tooling/renovate-runner/commit/67ef7ee77fcc84430fb784b888468e858380f879))
