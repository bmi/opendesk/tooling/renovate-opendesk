<!--
SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
SPDX-License-Identifier: Apache-2.0
-->

# Renovate

This repository contains the configuration for automated dependency updates via [Renovate](https://github.com/renovatebot/renovate).

## Usage

The solution is currently used to update the Helmfile / Helm chart based deployment automation for [openDesk](https://gitlab.opencode.de/bmi/opendesk/deployment/sovereign-workplace) only.
Therefore a custom scheduled GitLab pipeline approach without [onboarding](https://docs.renovatebot.com/getting-started/installing-onboarding/) or [shared configuration presets](https://docs.renovatebot.com/getting-started/use-cases/#configuration-presets) is implemented.

Renovate scans for [Helm chart](https://helm.sh/docs/topics/charts/) updates as well as for updates of the related container images.
If any updates are available, merge requests are created accordingly.

To enable the correct lookup and merge request creation for these dependency updates, additional metadata has to be provided in the respective source files as comments.

A so called [Dependency Dashboard](https://docs.renovatebot.com/getting-started/use-cases/#on-demand-updates-via-dependency-dashboard) is created in the scanned repository's issue list. This issue offers an overview of currently open merge requests created by Renovate.

### Metadata

#### Container images

All container images of [openDesk](https://gitlab.opencode.de/bmi/opendesk/deployment/sovereign-workplace) are specified [here](https://gitlab.opencode.de/bmi/opendesk/deployment/sovereign-workplace/-/blob/main/helmfile/environments/default/images.yaml).

To make Renovate find the right data sources and determine, how to manage updates in merge requests, the following attributes are required.

| Name             | Description                              | Values                           |
| ---------------- | ---------------------------------------- | -------------------------------- |
| `registryUrl`    | Registry URL that is checked for updates |                                  |
| `dependencyType` | Used to group updates in merge requests  | `external`,`supplier`,`platform` |

Example:

```
nextcloud:
    # renovate:
    # registryUrl=https://docker.io
    # dependencyType=vendor
    repository: "nextcloud"
    tag: "27.1.1-apache@sha256:47325758ffcd54563021e697905aaba6aac8c21bceefb245c67d40194813ce39"
    # @supplier: "Nextcloud Community"
```

#### Helm charts

All Helm charts of [openDesk](https://gitlab.opencode.de/bmi/opendesk/deployment/sovereign-workplace) are specified [here](https://gitlab.opencode.de/bmi/opendesk/deployment/sovereign-workplace/-/tree/main/helmfile/apps).

To make Renovate find the right data sources and determine, how to manage updates in merge requests, the following attributes are required in the related [Helmfile](https://github.com/helmfile/helmfile).

| Name             | Description                                   | Values                           |
| ---------------- | --------------------------------------------- | -------------------------------- |
| `registryUrl`    | Registry URL that is checked for updates      |                                  |
| `packageName`    | Used for looking up dependency versions       |                                  |
| `dataSource`     | Tells Renovate how to search for new versions | `docker`,`helm`                  |
| `dependencyType` | Used to group updates in merge requests       | `external`,`supplier`,`platform` |

Example:

```
releases:
  # renovate:
  # registryUrl=https://collaboraonline.github.io/online
  # packageName=collabora-online
  # dataSource=helm
  # dependencyType=supplier
  - name: "collabora-online"
    chart: "collabora-online-repo/collabora-online"
    version: "1.0.2"
    ...
```

### Environment variables

#### Scheduled pipeline

| Name                                  | Description                                                        | Required           | Documentation                                                                        |
| ------------------------------------- | ------------------------------------------------------------------ | ------------------ | ------------------------------------------------------------------------------------ |
| `RENOVATE_BASE_DIR`                   | Base directory for Renovate to store local files                   |                    | [Link](https://docs.renovatebot.com/self-hosted-configuration/#basedir)              |
| `RENOVATE_BASE_BRANCHES`              | Branches that should be processed by Renovate, defaults to `main`  |                    | [Link](https://docs.renovatebot.com/configuration-options/#basebranches)             |
| `RENOVATE_CACHE_DIR`                  | Directory where Renovate stores its cache                          |                    | [Link](https://docs.renovatebot.com/self-hosted-configuration/#cachedir)             |
| `RENOVATE_CONFIG_FILE`                | Configuration file for Renovate                                    | :white_check_mark: | [Link](https://docs.renovatebot.com/configuration-options/)                          |
| `RENOVATE_DEPENDENCY_DASHBOARD`       | Whether to create a "Dependency Dashboard" issue in the repository |                    | [Link](https://docs.renovatebot.com/configuration-options/#dependencydashboard)      |
| `RENOVATE_DEPENDENCY_DASHBOARD_TITLE` | Title for the Dependency Dashboard issue                           |                    | [Link](https://docs.renovatebot.com/configuration-options/#dependencydashboardtitle) |
| `RENOVATE_ENDPOINT`                   | Custom API endpoint to use                                         | :white_check_mark: | [Link](https://docs.renovatebot.com/self-hosted-configuration/#endpoint)             |
| `RENOVATE_IMAGE`                      | Container image of Renovate application                            | :white_check_mark: | [Link](https://github.com/renovatebot/renovate/pkgs/container/renovate)              |
| `RENOVATE_LOG_FILE`                   | Log file path                                                      |                    | [Link](https://docs.renovatebot.com/self-hosted-configuration/#logfile)              |
| `RENOVATE_LOG_FILE_LEVEL`             | Log file log level                                                 |                    | [Link](https://docs.renovatebot.com/self-hosted-configuration/#logfilelevel)         |
| `RENOVATE_REPOSITORY`                 | GitLab repository that should be processed                         | :white_check_mark: |                                                                                      |

#### CI/CD

| Name                           | Description                                                    | Required           | Documentation                                                                                |
| ------------------------------ | -------------------------------------------------------------- | ------------------ | -------------------------------------------------------------------------------------------- |
| `GITHUB_COM_TOKEN`             | Personal GitHub access token to overcome rate limit            |                    | [Link](https://docs.renovatebot.com/getting-started/running/#githubcom-token-for-changelogs) |
| `GITLAB_TOKEN`                 | GitLab group access token                                      | :white_check_mark: | [Link](https://docs.renovatebot.com/modules/platform/gitlab/)                                |
| `PROJECT_PATH_OPENDESK_CONFIG` | Project path of the shared openDesk GitLab CI/CD configuration | :white_check_mark: | [Link](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config)                     |
| `GITLAB_RUNNER_CLUSTER`        | Define which cluster to use for finding gitlab runners         | :white_check_mark: |                                                                                              |

## License

This project uses the following license: Apache-2.0

## Copyright

Copyright © 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
